# Expunge - delete duplicate and obsolete files

> **expunge** (verb)
>
> (transitive, computing) To delete permanently (e-mail etc.) that was
> previously marked for deletion but still stored.
>
> \- [wiktionary](https://en.wiktionary.org/wiki/expunge)

`expunge` is intended to remove the oldest files from backup or log
directories, while ensuring that...

- *consecutive duplicates* are removed (see below for details)
- the requested number of files are retained

## Internal Process

1. sort the selected files
2. remove ***newer*** *consecutive duplicates*
3. remove the oldest files until no more than the requested number of files remain

Note: Only the files named on the command line (or provided via STDIN) are
evaluated and removed, all other files in the directory are left uneffected.

### Example

I back up some of my Raspbery Pi projects by tarring or zipping their
configuration and data files on a regular basis. The resulting, time-stamped
files are then stored in a central backup directory and occasionally copied to
a separate NAS.

Roughly speaking, many backup strategies can be summarised something like
this:

```
% umask 077
% zip $backup_dir/backup-$(date -u +%Y%m%d-%H%M%S).zip $project_dir
% ls $backup_dir/*.zip
-rw-------  1 steve  staff   1.2G Oct  1 00:00 backup-20201001-230000.zip
-rw-------  1 steve  staff   1.2G Oct  2 00:00 backup-20201002-230000.zip
-rw-------  1 steve  staff   1.2G Oct  3 00:00 backup-20201003-230000.zip
-rw-------  1 steve  staff   1.2G Oct  4 00:00 backup-20201004-230000.zip
-rw-------  1 steve  staff   1.2G Oct  5 00:00 backup-20201005-230000.zip
-rw-------  1 steve  staff   1.2G Oct  6 00:00 backup-20201006-230000.zip
-rw-------  1 steve  staff   1.2G Oct  7 00:00 backup-20201007-230000.zip
...
```

The only thing missing is a way to clean up. Especially on small systems, this
strategy can quickly lead to 'no space left on device' errors.

`expunge -keep 3 $backup_dir/*.zip` will delete all but the three most recent,
unique files.

Running expunge regularly (as as the last step of your backup process) will
keep your archive within the limits you set it.

## What Does 'consecutive duplicates' Mean?

New backups are typically created regardless of whether or not any changes were
made. This can lead to multiple, identical backup files being collected.
`expunge` checks for files which are identical to their predecessors, and
removes the more recent duplicate.

This example should demonstrate the princple:

| file  | content | action | reason                 |
| ----  | ------- | ------ | -------                |
| 1.zip | "A"     | keep   | first occurance of "A" |
| 2.zip | "A"     | delete | duplicate of 1.zip     |
| 3.zip | "B"     | keep   | first occurance of "B" |
| 4.zip | "A"     | keep   | not the same as 3.zip  |
| 5.zip | "A"     | delete | duplicate of 4.zip     |
| 6.zip | "C"     | keep   | not the same as 5.zip  |
| 7.zip | "B"     | keep   | not the same as 6.zip  |
| 8.zip | "B"     | delete | duplicate of 7.zip     |
| 9.zip | "B"     | delete | duplicate of 8.zip     |

Which would leave us with:

| file  | content |
| ----  | ------- |
| 1.zip | "A"     |
| 3.zip | "B"     |
| 4.zip | "A"     |
| 6.zip | "C"     |
| 7.zip | "B"     |

Note that duplicates can still occur, if the system (somehow) reverted to a
previous state. `expunge` uses this algorithm to ensure that recoving a backup
for a given date will restore the system to the *most-likely* state on that date.

In the above example, `1.zip` represents the system state from the time that
`1.zip` was created until *some time between when `2.zip` and `3.zip`* were
created. Likewise, `4.zip` contains the same data, as this was the state of the
system between when `4.zip` and `6.zip` were created.

Uniqueness is checked via SHA1 checksums of each file's contents.

Uniqueness checks can be suppressed by using the `-no-unique` option.

## Recommended Usage

1. name your backups or logs so that...
  - they can be easily selected via simple patterns
  - collisions are avoided
    - I prefer UTC timestamps in ISO 8601 format, e.g. `date -u +%Y%m%d-%H%M%S`
  - e.g.: `{project}-backup-{timestamp}.zip`
2. decide how many copies of each type of file you wish to keep
3. create new backups, *then* run `expunge`
  - this might delete the backup just created if it is the same as the previous backup

### For *Small* Numbers of Files

Suitable for most situations

`expunge -keep 20 /backups/*.zip`

### For *Large* Numbers of Files

`find /backups -maxdepth 1 -print | expunge -keep 100 -`

Note: `expunge` is intended for cleaning files within a single directory.
Using `find` to find unrelated files is probably a bad idea.

**IMPORTANT:** ***never*** use `xargs` to run `expunge`! `expunge` *must*
process the full list of files to be cleaned in a single process!

| Warning | Example | Remarks |
| ------- | ------- | ------- |
| ***Dangerous:*** | `find ... \| xargs expunge` | don't do this... *ever!* |
| ***Optimal:***   | **`find ... \| expunge -`** | remove `xargs` and add the `-` (*stdin-dash*) option |

## Installation

### Dependencies

- \*nix-like OS
- perl 5

### Testing

- `prove perl/test.t`
    - run all tests
    - print a short summary only
    - discard `test_run/` directory if all tests passed

- `perl/test.t`
    - run all tests
    - print all test output
    - keep `test_run/` directory for manual inspection

### Installation

Copy `perl/expunge` into a directory in your `$PATH`, e.g.: `~/bin`.

## Background

`expunge` started life as a perl one-liner used to remove consecutive duplicate
backups:

```
ls -lr $backup_dir/backup*.zip \
    | perl -MDigest::SHA -lne 'BEGIN{$h=Digest::SHA()};$d=$h->addfile($_)->digest();if($d eq $p){unlink}else{print"$_\n"};$p=$d' \
    | head -n-10 \
    | xargs -r rm
```

It has since grown into a self-contained perl script which replaces the above shell pipe completely.

## Copyright

© Stephen Riehm, 2021, Artistic License
