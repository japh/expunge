.PHONY:	features feature-rules feature-scenarios feature-steps unique-steps
.SILENT:

# BDD / Gherkin support
# 	Each of the following rules provide an overview of the defined BDD
# 	features, in varying levels of detail.
# 	TODO: 2021-10-23 replace grep with something more intelligent, 
# 	ie: something that actually parses gherkin and can provide a structure
# 	    for reporting test success/failure status
features:
	grep -R -h '^\(Ability\|Feature\):' features/

feature-rules:
	grep -R -h '^\(Ability\|Feature\|\s\+Rule\):' features/

# should include examples: tables (1 test per table row)
feature-scenarios:
	grep -R -h '^\(\(\(Ability\|Feature\|Business Need\)\|\s\+\(Rule\|Scenario\|Example\|Scenario Outline\)\):\)' features/

feature-steps:
	grep -R -h '^\(\(\(Ability\|Feature\)\|\s\+\(Rule\|Background\|Scenario\):\)\|\s*\(Given\|When\|Then\|And\|But\|\*\)\s\+\)' features/

unique-steps:
	grep -R -h '^\s\+\(Given\|When\|Then\|And\|But\|\*\)\s\+' features/ \
		| perl -pe 's/^\s+|\s+(\v)/$$1/g' \
		| perl -pe 's/(^|\s)(?:"[^"]*"|<[^>]*>|[+-]?\d*(?:\.\d+)?)(\s|\v)/$${1}{}$${2}/g' \
		| perl -pe 's{^(And|But|\*)\b}{($$current // $$1)}e; /^(\w+)/; $$current = $$1' \
		| sort -i -u
