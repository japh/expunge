#!/usr/bin/env perl

#
# Test suite for 'expunge'
#
# Usage:
#
#   prove ./test.t
#
#       'prove' runs all tests and prints a summary of the results.
#       If all goes well, the directory will be automatically removed after testing.
#       If any tests fail, their directories will be kept for manual inspection.
#
#   ./test.t
#
#       Running test.t directly runs all tests and displays their output on STDOUT.
#
# Testing Process:
#
#       Since 'expunge' is a file-system utility, all tests are run in a
#       temporary directory structure.
#
#       test.t always creates the following temporary structure for testing:
#
#           ${CWD}/
#               test_run/
#                   {time-stamp}/
#                       {test number}/
#                           files/...
#                           step-{step number}.{type}
#
#       Each test gets its own directory, containing a 'files/' directory
#       with a set of generated files to be 'expunged'.
#
#       While testing, the output of each step is captured in a
#       number of step-{number}... files.
#
#       When run via 'prove test.t', all temporary files are deleted whenever a
#       test succeeds.
#       When test.t is run directly, or whenever a test fails, then the
#       temporary directories are kept for manual inspection.
#
#       After testing, the test_run/ directory can be deleted.
#

use v5.10;
use strict;
use warnings;

# core modules since perl 5.10 (and earlier)
use Test::More;
use Test::Differences;

use FindBin qw( $RealBin );
our $expunge       = "$RealBin/expunge";
our $date_time_fmt = "%Y%m%d-%H%M%S";

my @tests = (

            {
            line  => __LINE__,
            name  => "call without args",
            when  => [ '{expunge}' ],
            then  => {
                     status => qr{exit 2 - failed},
                     stderr => qr{no files to expunge.*^Usage}ms,
                     },
            },

            {
            line  => __LINE__,
            name  => "version",
            when  => [ '{expunge} -version' ],
            then  => {
                     stdout => qr{expunge\s+v\d+\.\d+\.\d+},
                     },
            },

            {
            line  => __LINE__,
            name  => "usage",
            when  => [ '{expunge} -usage' ],
            then  => {
                     status => qr{exit 2 - failed},
                     stderr => qr{Usage},
                     },
            },

            {
            line  => __LINE__,
            name  => "help",
            when  => [ '{expunge} -help' ],
            then  => {
                     stdout => qr{SYNOPSIS.*DESCRIPTION}ms,
                     },
            },

            {
            line  => __LINE__,
            name  => "empty dir with unexpanded glob",
            when  => [ '{expunge} {dir}/*' ],
            then  => {},
            },

            {
            line  => __LINE__,
            name  => "empty dir with unexpanded glob",
            when  => [ '{expunge} -dry-run "*"' ],
            then  => {},
            },

            {
            line  => __LINE__,
            name  => "dir with 1 file",
            given => { '{id}.txt' => 1 },
            when  => [ '{expunge} {dir}/*' ],
            then  => {
                     file_count => 1,
                     files      => {
                                   "1.txt" => 1,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "dir with enough files to expunge",
            given => { '{id}-{data}.log' => 10 },
            when  => [ '{expunge} {dir}/*' ],
            then  => {
                     file_count => 4,
                     files      => {
                                   "01-0.log" => 1,
                                   "02-0.log" => 0,
                                   "03-1.log" => 1,
                                   "04-1.log" => 0,
                                   "05-1.log" => 0,
                                   "06-2.log" => 1,
                                   "07-2.log" => 0,
                                   "08-2.log" => 0,
                                   "09-0.log" => 1,
                                   "10-0.log" => 0,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "keep 10 most recent successively unique files by default",
            given => { '{id}-{data}.log' => 50 },
            when  => [ '{expunge} {dir}/*' ],
            then  => {
                     file_count => 10,
                     files      => {
                                   "15-2.log" => 0, # too old
                                   "16-2.log" => 0, # duplicate of 15-1.log
                                   "17-2.log" => 0, # duplicate of 15-1.log
                                   "18-0.log" => 0, # too old
                                   "19-0.log" => 0,
                                   "20-0.log" => 0,
                                   "21-1.log" => 1, # 10th most recent unique file
                                   "22-1.log" => 0, # duplicate of 21-1.log
                                   "23-1.log" => 0, # duplicate of 22-1.log
                                   "24-2.log" => 1, # 9th most recent unique file
                                   "25-2.log" => 0,
                                   "26-2.log" => 0,
                                   "27-0.log" => 1,
                                   "28-0.log" => 0,
                                   "29-0.log" => 0,
                                   "30-1.log" => 1,
                                   "31-1.log" => 0,
                                   "32-1.log" => 0,
                                   "33-2.log" => 1,
                                   "34-2.log" => 0,
                                   "35-2.log" => 0,
                                   "36-0.log" => 1,
                                   "37-0.log" => 0,
                                   "38-0.log" => 0,
                                   "39-1.log" => 1,
                                   "40-1.log" => 0,
                                   "41-1.log" => 0,
                                   "42-2.log" => 1,
                                   "43-2.log" => 0,
                                   "44-2.log" => 0,
                                   "45-0.log" => 1,
                                   "46-0.log" => 0,
                                   "47-0.log" => 0,
                                   "48-1.log" => 1,
                                   "49-1.log" => 0,
                                   "50-1.log" => 0,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "keep 5 most recent files, disregarding their contents",
            given => { '{id}-{data}.log' => 10 },
            when  => [ '{expunge} -keep 5 --no-unique {dir}/*' ],
            then  => {
                     file_count => 5,
                     files      => {
                                   "01-0.log" => 0,
                                   "02-0.log" => 0,
                                   "03-1.log" => 0,
                                   "04-1.log" => 0,
                                   "05-1.log" => 0,
                                   "06-2.log" => 1,
                                   "07-2.log" => 1,
                                   "08-2.log" => 1,
                                   "09-0.log" => 1,
                                   "10-0.log" => 1,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "keep 5 most recent files, sorted by name only (keep duplicates)",
            given => { '{data}-{id}.log' => 30 },
            when  => [ '{expunge} -keep 5 -no-unique -sort-by name {dir}/*' ],
            then  => {
                     file_count => 5,
                     files      => {
                                   "0-01.log" => 0,
                                   "0-02.log" => 0,
                                   "0-29.log" => 0,
                                   "1-03.log" => 0,
                                   "1-04.log" => 0,
                                   "1-23.log" => 0,
                                   "1-30.log" => 0,
                                   "2-06.log" => 0,
                                   "2-15.log" => 0,
                                   "2-16.log" => 1,
                                   "2-17.log" => 1,
                                   "2-24.log" => 1,
                                   "2-25.log" => 1,
                                   "2-26.log" => 1,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "keep 5 most recent files, sorted by name only (keep duplicates)",
            given => { '{data}-{id}.log' => 30 },
            when  => [ '{expunge} -keep 5 -no-unique -sort-by age {dir}/*' ],
            then  => {
                     file_count => 5,
                     files      => {
                                   "1-23.log" => 0,
                                   "2-24.log" => 0,
                                   "2-25.log" => 0,
                                   "2-26.log" => 1,
                                   "0-27.log" => 1,
                                   "0-28.log" => 1,
                                   "0-29.log" => 1,
                                   "1-30.log" => 1,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "pipe names of files to be expunged",
            given => { '{data}-{id}.log' => 30 },
            when  => [ 'find {dir} -maxdepth 1 -type f -name \*.log | {expunge} -keep 5 -no-unique -sort-by age -' ],
            then  => {
                     file_count => 5,
                     files      => {
                                   "1-23.log" => 0,
                                   "2-24.log" => 0,
                                   "2-25.log" => 0,
                                   "2-26.log" => 1,
                                   "0-27.log" => 1,
                                   "0-28.log" => 1,
                                   "0-29.log" => 1,
                                   "1-30.log" => 1,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "do nothing if no names are provided via STDIN",
            when  => [ 'echo | {expunge} -verbose -' ],
            then  => {
                     stdout => qr{
                                 Got \s+ 0 \s+ file \s+ names \s+ from \s+ STDIN
                                 .*
                                 No \s+ files \s+ processed
                                 }msx,
                     },
            },

            {
            line  => __LINE__,
            name  => "verbose explains why each file was kept or deleted",
            given => { '{id}-{data}.log' => 50 },
            when  => [ '{expunge} -verbose {dir}/*' ],
            then  => {
                     file_count => 10,
                     stdout     => qr{
                                   .* 15-2.log \s+ - \s+ deleted, \s+ obsolete  \v+
                                   .* 16-2.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/15-2.log \v+
                                   .* 17-2.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/15-2.log \v+
                                   .* 18-0.log \s+ - \s+ deleted, \s+ obsolete  \v+
                                   .* 19-0.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/18-0.log \v+
                                   .* 20-0.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/18-0.log \v+
                                   .* 21-1.log \s+ - \s+ kept \v+
                                   .* 22-1.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/21-1.log \v+
                                   .* 23-1.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/21-1.log \v+
                                   .* 24-2.log \s+ - \s+ kept \v+
                                   # ---
                                   .* 48-1.log \s+ - \s+ kept \v+
                                   .* 49-1.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/48-1.log \v+
                                   .* 50-1.log \s+ - \s+ deleted, \s+ duplicate \s+ of \s+ \S+/48-1.log \v+
                                   }xms,
                     files      => {
                                   "15-2.log" => 0, # too old
                                   "16-2.log" => 0, # duplicate of 15-1.log
                                   "17-2.log" => 0, # duplicate of 15-1.log
                                   "18-0.log" => 0, # too old
                                   "19-0.log" => 0,
                                   "20-0.log" => 0,
                                   "21-1.log" => 1,
                                   "22-1.log" => 0, # duplicate of 21-1.log
                                   "23-1.log" => 0, # duplicate of 22-1.log
                                   "24-2.log" => 1,
                                   "25-2.log" => 0,
                                   "26-2.log" => 0,
                                   "27-0.log" => 1,
                                   "28-0.log" => 0,
                                   "29-0.log" => 0,
                                   "30-1.log" => 1,
                                   "31-1.log" => 0,
                                   "32-1.log" => 0,
                                   "33-2.log" => 1,
                                   "34-2.log" => 0,
                                   "35-2.log" => 0,
                                   "36-0.log" => 1,
                                   "37-0.log" => 0,
                                   "38-0.log" => 0,
                                   "39-1.log" => 1,
                                   "40-1.log" => 0,
                                   "41-1.log" => 0,
                                   "42-2.log" => 1,
                                   "43-2.log" => 0,
                                   "44-2.log" => 0,
                                   "45-0.log" => 1,
                                   "46-0.log" => 0,
                                   "47-0.log" => 0,
                                   "48-1.log" => 1,
                                   "49-1.log" => 0,
                                   "50-1.log" => 0,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "--name-only test, stdout only",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} --name-only {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     ^\S+/2-0\.log\v+
                                     ^\S+/4-1\.log\v+
                                     ^\S+/5-1\.log\v+
                                     }xm,
                     file_count => 5,
                     },
            },

            {
            line  => __LINE__,
            name  => "--name-only --print0 test, stdout only",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} --name-only --print0 {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     \S+/2-0\.log\0
                                     \S+/4-1\.log\0
                                     \S+/5-1\.log\0 # trailing \0 seems to be OK
                                     }xm,
                     file_count => 5,
                     },
            },

            {
            line  => __LINE__,
            name  => "--print0 (without --name-only), same as --name-only --print0",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} --print0 {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     \S+/2-0\.log\0
                                     \S+/4-1\.log\0
                                     \S+/5-1\.log\0 # trailing \0 seems to be OK
                                     }xm,
                     file_count => 5,
                     # but expunge no files should be deleted with --name-only
                     files      => {
                                   "1-0.log" => 1,
                                   "2-0.log" => 1,
                                   "3-1.log" => 1,
                                   "4-1.log" => 1,
                                   "5-1.log" => 1,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "--verbose and --name-only => --name-only wins",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} --name-only --verbose {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     \S+/2-0\.log\v+
                                     \S+/4-1\.log\v+
                                     \S+/5-1\.log\v+
                                     }xm,
                     file_count => 5,
                     },
            },

            {
            line  => __LINE__,
            name  => "--dry-run should say WHAT would happen, without doing anything",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} --dry-run {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     \A
                                     \S+/1-0\.log \s+ - \s+ would \s+ keep   .*\v+
                                     \S+/2-0\.log \s+ - \s+ would \s+ delete .* duplicate .*\v+
                                     \S+/3-1\.log \s+ - \s+ would \s+ keep   .*\v+
                                     \S+/4-1\.log \s+ - \s+ would \s+ delete .* duplicate .*\v+
                                     \S+/5-1\.log \s+ - \s+ would \s+ delete .* duplicate .*\v+
                                     \z
                                     }xm,
                     file_count => 5,
                     },
            },

            {
            line  => __LINE__,
            name  => "verbose --dry-run should say WHAT would happen, without doing anything",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} -verbose --dry-run {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     \A
                                     Checking.*\v+
                                     \S+/1-0\.log \s+ - \s+ would \s+ keep   .*\v+
                                     \S+/2-0\.log \s+ - \s+ would \s+ delete .* duplicate .*\v+
                                     \S+/3-1\.log \s+ - \s+ would \s+ keep   .*\v+
                                     \S+/4-1\.log \s+ - \s+ would \s+ delete .* duplicate .*\v+
                                     \S+/5-1\.log \s+ - \s+ would \s+ delete .* duplicate .*\v+
                                     would \s+ have .*\v+
                                     \z
                                     }xm,
                     file_count => 5,
                     },
            },

            {
            line  => __LINE__,
            name  => "verbose should delete and say what is happening",
            given => { '{id}-{data}.log' => 5 },
            when  => [ '{expunge} -verbose {dir}/*' ],
            then  => {
                     # expunge should report the names of obsolete files
                     stdout     => qr{
                                     \A
                                     Checking.*\v+
                                     \S+/1-0\.log \s+ - \s+ kept    .*\v+
                                     \S+/2-0\.log \s+ - \s+ deleted .* duplicate .*\v+
                                     \S+/3-1\.log \s+ - \s+ kept    .*\v+
                                     \S+/4-1\.log \s+ - \s+ deleted .* duplicate .*\v+
                                     \S+/5-1\.log \s+ - \s+ deleted .* duplicate .*\v+
                                     kept \s+ 2 \s+ and \s+ deleted \s+ 3 \s+ files \v+
                                     \z
                                     }xm,
                     file_count => 2,
                     files      => {
                                   "1-0.log" => 1,
                                   "2-0.log" => 0,
                                   "3-1.log" => 1,
                                   "4-1.log" => 0,
                                   "5-1.log" => 0,
                                   },
                     },
            },

            {
            line  => __LINE__,
            name  => "selective deletion - delete different number of log and backup files",
            given => {
                     '{id}-{data}.log'         => 10,
                     'archive-{id}-{data}.zip' => 10,
                     'backup-{id}-{data}.zip'  => 10,
                     },
            when  => [
                     '{expunge} -keep 1 {dir}/*.log',
                     '{expunge} -keep 5 {dir}/backup-*.zip',
                     ],
            then  => {
                     file_count => 1    # -keep 1 log file
                                 + 10   # all archive files left untouched
                                 + 4,   # 4 of 5 backups kept: 1, 3, 6, 9
                     files      => {
                                   # log files
                                   "08-2.log" => 0,
                                   "09-0.log" => 1,
                                   "10-0.log" => 0,
                                   # archive files (all kept!)
                                   "archive-01-0.zip" => 1,
                                   "archive-02-0.zip" => 1,
                                   "archive-03-1.zip" => 1,
                                   "archive-04-1.zip" => 1,
                                   "archive-05-1.zip" => 1,
                                   "archive-06-2.zip" => 1,
                                   "archive-07-2.zip" => 1,
                                   "archive-08-2.zip" => 1,
                                   "archive-09-0.zip" => 1,
                                   "archive-10-0.zip" => 1,
                                   # backup files
                                   "backup-01-0.zip" => 1,
                                   "backup-02-0.zip" => 0,
                                   "backup-03-1.zip" => 1,
                                   "backup-04-1.zip" => 0,
                                   "backup-05-1.zip" => 0,
                                   "backup-06-2.zip" => 1,
                                   "backup-07-2.zip" => 0,
                                   "backup-08-2.zip" => 0,
                                   "backup-09-0.zip" => 1,
                                   "backup-10-0.zip" => 0,
                                   },
                     },
            },

            );

my $session = test_session->new(
                               cleanup     => $ENV{HARNESS_ACTIVE} ? 1 : 0,
                               total_tests => scalar @tests,
                               env         => {
                                              expunge => $expunge,  # the script being tested
                                              },
                               );

my $check_wip = grep { $_->{wip} } @tests;
my $skipped   = 0;
foreach my $test ( @tests )
    {
    my $test_case = $session->new_test_case(
                                           line => $test->{line},
                                           name => $test->{name},
                                           );

    if( $check_wip and not $test->{wip} )
        {
        $skipped++;
        next;
        }

    subtest $test_case->name(),
        => sub {
                my $given = $test->{given};
                my $when  = $test->{when};
                my $then  = $test->{then};

                my $errs = 0;

                # given:
                $test_case->create_test_files( %{ $given // {} } );

                # when:
                $test_case->run_ls_step();
                foreach my $step ( @{ $when // [] } )
                    {
                    $test_case->run_cmd_step( $step );
                    }
                $test_case->run_ls_step();

                # then
                $errs += 1 - like( $test_case->exit_status(), $then->{status} // qr/0 - ok/, 'exit code' );
                $errs += 1 - like( $test_case->stdout(),      $then->{stdout} // qr/\A\z/m,  'stdout'    );
                $errs += 1 - like( $test_case->stderr(),      $then->{stderr} // qr/\A\z/m,  'stderr'    );

                exists $then->{file_count}
                    and $errs += 1 - is( scalar( $test_case->files() ), $then->{file_count}, 'file count' );

                my $have_file = { map { s/\S+\s+//r => 1 } $test_case->files() };
                my $want_file = $then->{files} // {};
                foreach my $file ( sort keys %{$want_file} )
                    {
                    $errs += 1 - is(
                               $have_file->{$file} // 0,
                               $want_file->{$file},
                               sprintf( "%s %s",
                                       $file,
                                       $want_file->{$file} ? 'kept' : 'removed',
                                       ),
                               );
                    }

                if( $errs )
                    {
                    $test_case->failed();
                    $session->failed();
                    }
                };
    }

if( $skipped )
    {
    diag( sprintf "WARNING: only tested WIP tests - skipped %d tests (remove WIP markers before committing)", $skipped );
    }

done_testing();
exit;

####################################################################################################
#
# Support Packages
#

####################
#
# test_session encapsulates temporary directory creation and deletion for the
# entire test run.
#

package test_session;

use strict;
use warnings;

# core modules since perl 5.10 (and earlier)
use File::Path qw( make_path remove_tree );
use POSIX      qw( strftime );

# Parameters:
#   total_tests     total number of tests (for 0-padding test numbers)
#   cleanup         [0|1] automatically delete the session dir
#                   default: 1
#   env             hash of keyword => value mappings for expansion in test steps.
#                   e.g.: when => [ '{expunge} {dir}/*.log' ]
sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;
    $self->{cleanup}           = $param->{cleanup} // 1;
    $self->{env}               = $param->{env}     // {};
    $self->{test_nr}           = 0;
    $self->{session_dir}       = sprintf( "test_run/%s.%d", strftime( $date_time_fmt, gmtime()), $$ );
    $self->{case_dir_template} = sprintf( "%s/%%0%dd",
                                        $self->{session_dir},
                                        length( $param->{total_tests} // 0 ),
                                        );

    return $self;
    }

# call ->failed() to prevent auto-removal if errors occurred
sub failed { my $self = shift; $self->{failed} = 1; }

sub DESTROY
    {
    my $self = shift;

    if( $self->{cleanup} and not $self->{failed} )
        {
        remove_tree( $self->{session_dir} );
        }

    # always cleanup empty directories
    rmdir "session_dir";
    rmdir "test_run";
    }

sub new_test_case
    {
    my $self  = shift;
    my $param = { @_ };

    my $case_dir = sprintf( $self->{case_dir_template}, ++$self->{test_nr} );

    make_path( $case_dir );

    return test_case->new(
                         name     => $param->{name},
                         line     => $param->{line},
                         base_dir => $case_dir,
                         cleanup  => $self->{cleanup},
                         env      => {
                                     %{ $self->{env}  // {} },  # session environment
                                     %{ $param->{env} // {} },  # additional env from caller
                                     },
                         );
    }

####################
#
# test_case creates and destroys directories and captures output from steps for a single test-case
#

package test_case;

use strict;
use warnings;

# core modules since perl 5.10 (and earlier)
use File::Path qw( remove_tree );
use POSIX qw( strftime );

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self  = bless {}, $class;

    $self->{name}      = $param->{name};
    $self->{line}      = $param->{line};
    $self->{base_dir}  = $param->{base_dir};
    $self->{cleanup}   = $param->{cleanup};
    $self->{env}       = $param->{env} // {};
    $self->{files_dir} = join( "/", $self->{base_dir}, "files" );
    $self->{step}      = 0;

    # provide '{dir}' expansion to test definitions
    $self->{env}{dir} = $self->{files_dir};

    return $self;
    }

# call ->failed() to prevent auto-removal if errors occurred
sub failed { my $self = shift; $self->{failed} = 1; }

sub DESTROY
    {
    my $self = shift;

    if( $self->{cleanup} and not $self->{failed} )
        {
        remove_tree( $self->{base_dir} );
        }

    # always cleanup empty directories
    rmdir $self->{base_dir};
    }

sub name            { my $self = shift; return sprintf "%s from line %d\n", $self->{name}, $self->{line} }
sub next_step_name  { my $self = shift; return sprintf "step-%d", ++$self->{step} }

sub create_test_files
    {
    my $self  = shift;
    my $files = { @_ };   # { <format> => count }

    my $files_dir = $self->{files_dir};

    mkdir $files_dir;

    my $now = time;

    foreach my $file_template ( sort keys %{$files} )
        {
        my $file_count = $files->{$file_template};
        my $id_digits  = length $file_count;
        foreach my $i ( 1 .. $file_count )
            {
            my $env = {
                      id   => sprintf( "%0*d", $id_digits, $i ),
                      data => $i / 3 % 3, # pseudo-random, repeating data
                      };
            my $file = join( "/",
                            $files_dir,
                            $file_template =~ s:{(\w+)}:$env->{$1}//$&:egr,
                            );
            open( my $fd, ">", $file )
                or die sprintf "cannot create '%s' - %s\n", $file, $!;
            printf $fd $env->{data};
            close $fd;

            my $timestamp = $now - ( $file_count - $i );
            utime $timestamp, $timestamp, $file;
            }
        }

    return;
    }

sub run_ls_step
    {
    my $self = shift;

    my $base_dir  = $self->{base_dir};
    my $files_dir = $self->{files_dir};

    opendir( my $dh, $files_dir )
        or die sprintf "cannot open directory %s - %s\n", $files_dir, $!;
    $self->{files} = [
                     sort
                     map  {
                          sprintf( "%s %s",
                                 strftime( $date_time_fmt, gmtime( (stat( "$files_dir/$_" ))[9] ) ),
                                 $_,
                                 );
                          }
                     grep { not /^\./ }
                     readdir( $dh )
                     ];
    closedir $dh;

    if( @{$self->{files}} )
        {
        my $step_out_file = sprintf( "%s/%s.ls", $base_dir, $self->next_step_name() );
        open( my $out_fh, ">", $step_out_file )
            or die sprintf "cannot create '%s' - %s\n", $step_out_file, $!;
        print $out_fh "$_\n" foreach @{$self->{files}};
        close $out_fh;
        }

    return;
    }

sub run_cmd_step
    {
    my $self     = shift;
    my $cmd_line = shift;

    my $base_dir  = $self->{base_dir};
    my $env       = $self->{env};
    my $step_name = $self->next_step_name();
    my $cmd_file  = sprintf( "%s/%s.cmd",     $base_dir, $step_name );
    my $exit_file = sprintf( "%s/%s.exit",    $base_dir, $step_name );
    my $out_file  = sprintf( "%s/%s.stdout",  $base_dir, $step_name );
    my $err_file  = sprintf( "%s/%s.stderr",  $base_dir, $step_name );

    $cmd_line =~ s:{(\w+)}:$env->{$1}//$&:eg;

    open( my $cmd_fd, ">", $cmd_file )
        or die sprintf "cannot create %s - %s\n", $cmd_file, $!;
    print $cmd_fd "$cmd_line\n";
    close $cmd_fd;

    my $exit_code   = system( $cmd_line . " 1> $out_file 2>$err_file" );
    my $exit_status = ( $exit_code == -1  ) ? "failed to run"
                    : ( $exit_code & 0x7f ) ? sprintf( "killed %d - signal %d, %s core dump",
                                                        $exit_code >> 8,
                                                        $exit_code & 0x7f,
                                                        $exit_code & 0x80 ? "with" : "without"
                                                        )
                    : ( $exit_code != 0   ) ? sprintf( "exit %d - failed", $exit_code >> 8 )
                    : "exit 0 - ok";

    open( my $exit_fd, ">", $exit_file )
        or die sprintf "cannot create %s - %s\n", $exit_file, $!;
    printf $exit_fd "%s\n", $exit_status;
    close $exit_fd;

    $self->{exit_status} = $exit_status;
    $self->{stdout}      = slurp_or_delete( $out_file );
    $self->{stderr}      = slurp_or_delete( $err_file );
    }

sub slurp_or_delete
    {
    my $file = shift;
    open( my $fd, "<", $file )
        or die sprintf "cannot read %s - %s\n", $file, $!;
    my $data = join("\n", <$fd>);
    close $fd;
    unlink $file    if not length( $data );
    return $data;
    }

sub exit_status { my $self = shift; return $self->{exit_status}; }
sub stdout      { my $self = shift; return $self->{stdout};      }
sub stderr      { my $self = shift; return $self->{stderr};      }
sub files       { my $self = shift; return @{$self->{files}};    }
