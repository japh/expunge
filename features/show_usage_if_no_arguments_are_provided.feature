Ability: show usage if no arguments are provided

  Rule: delete no files when expunge is called without arguments

    Scenario: an empty file list is provided

      e.g. via STDIN or unexpanded file glob
      see also feature: do nothing if no files are found

       When expunge is run
        But no arguments are provided
       Then no files should be deleted

    Scenario: expunge is called without arguments

       When expunge is run
        But no arguments are provided
       Then a usage message should be shown

