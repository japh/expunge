Ability: warn about missing files

  Rule: produce a warning if named files cannot be found

    Scenario: files are named, which don't exist

      Given an empty directory
       When expunge is run
        And the file "missing.txt" is named
        But the file "missing.txt" does not exist
       Then a "file not found" warning should be produced
