Ability: delete obsolete files

  In order to reduce the space taken by 'simple backups' (zip files)
  I want to keep a known number of files
  and delete any files which are older.

  Rule: delete oldest files first

    Scenario: fewer files exist than the keep limit allows

      Given a list of files
            | 1.zip |
            | 2.zip |
            | 3.zip |
       When expunge is run
        And the keep limit is set to 5
       Then no files should be deleted

    Scenario: more files exist than the keep limit allows

      Given a list of files
            | 1.zip |
            | 2.zip |
            | 3.zip |
            | 4.zip |
            | 5.zip |
            | 6.zip |
            | 7.zip |
            | 8.zip |
       When expunge is run
        And the keep limit is set to 5
       Then the oldest files should be deleted
        And the files kept should be
            | 4.zip |
            | 5.zip |
            | 6.zip |
            | 7.zip |
            | 8.zip |

