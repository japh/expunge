Ability: do nothing if no files are found

  Rule: do nothing if no files are found

    Empty directories are not a problem, there's nothing to do, so do nothing

    Scenario: no file names provided via STDIN

      Given an empty list of files
       When expunge is run
        And the 'read from STDIN' option is provided
        But no file names are provided via STDIN
       Then no files should be deleted
        And no usage should be displayed

    Scenario: an unexpanded glob pattern is provided

      shells tend to pass the glob pattern to programs if they don't match any
      files. e.g.: `echo /backups/*.zip` => `/backups/*.zip`

      Given an empty directory
       When expunge is run
        And a cli argument is an <unexpanded glob>
        And the directory part of the glob <exists?>
       Then no files should be deleted
        And <usage?> usage should be displayed

      Examples:
        | unexpanded glob | exists? | usage? | comment           |
        | /backups/*.zip  | yes     | no     |                   |
        | *.zip           | yes     | no     | assumed to be CWD |
        | /bupacks/*.zip  | no      | yes    | user made a typo? |
