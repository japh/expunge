Ability: sort files by age or name

  Regardless of naming convention, files should be sorted by age to determine
  which files are to be deleted.

  Rule: sort files by age by default

    If files are named using simple integers, for example, then their
    lexicographical order will not always match their chronological order.
    By default, the file's relative age should be used to determine which are
    oldest, and therefore, obsolete.

    Scenario: sort files by age

      Given a list of files and ages
            | 1.zip  | 8 days |
            | 10.zip | 5 days |
            | 11.zip | 4 days |
            | 8.zip  | 7 days |
            | 9.zip  | 6 days |
       When expunge is run
       Then the files should be evaluated in chronological order
            | 1.zip  | 8 days |
            | 8.zip  | 7 days |
            | 9.zip  | 6 days |
            | 10.zip | 5 days |
            | 11.zip | 4 days |

  Rule: files may be sorted by name

    If file modificiation times have been corrupted, it may be helpful to sort
    files lexicographical by their names.

    Scenario: sort files by name

      Given a list of files and ages
            | 1.zip  | 8 days |
            | 8.zip  | 7 days |
            | 9.zip  | 6 days |
            | 10.zip | 5 days |
            | 11.zip | 4 days |
       When expunge is run
        And sort-by-name is selected
       Then the files should be evaluated in lexicographical order
            | 1.zip  | 8 days |
            | 10.zip | 5 days |
            | 11.zip | 4 days |
            | 8.zip  | 7 days |
            | 9.zip  | 6 days |
