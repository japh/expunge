Ability: delete most recent consecutive duplicates

  Rule: consecutive duplicates should be removed

    Scenario: delete consecutive duplicate files

      Given multiple, consecutive files with content
            | 1.zip | a |
            | 2.zip | a |
            | 3.zip | a |
       When expunge is run
       Then only the oldest duplicate should be kept
            | 1.zip | a |

    Scenario: keep oldest copy of each consecutive group of duplicates

      Given multiple, consecutive files with content
            | 1.zip | a |
            | 2.zip | a |
            | 3.zip | a |
            | 4.zip | b |
            | 5.zip | b |
            | 6.zip | a |
       When expunge is run
        And the keep limit is set to 3
       Then only the oldest duplicate of each consecutive group of duplicates should be kept
            | 1.zip | a |
            | 4.zip | b |
            | 6.zip | a |

  Rule: the check for duplicates may be supressed

      Given multiple, consecutive files with content
            | 1.zip | a |
            | 2.zip | a |
            | 3.zip | a |
            | 4.zip | b |
            | 5.zip | b |
            | 6.zip | a |
       When expunge is run
        And the keep limit is set to 3
        And duplicate identification should be skipped
       Then only the most recent files should be kept
            | 4.zip | b |
            | 5.zip | b |
            | 6.zip | a |

